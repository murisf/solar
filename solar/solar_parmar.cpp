/* Jagdeep Parmar */
/* cs4410 hw4 */
/* 12/03/2014 */
#include <Windows.h>
#include <iostream>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <gl/glut.h>
#include "RGBPixmap.h"
#include "camera.h"

RGBpixmap sun;
RGBpixmap pix[9];
RGBpixmap moon;
RGBpixmap saturnRing;
Camera cam;

int planetMode = 0;
const int windowWidth = 1024;
const int windowHeight = 768;
const double zoom = 2;
static bool animation = false;
static float eyeX = 0;
static float eyeY = 0;
static float eyeZ = 100;
static double rotX = 0;
static double rotY = 0;
static double rotZ = 0;
bool movement = true;

Point3 look(0, 0, 0);
Vector3 up(0, 1, 0);

// radius of objects
const double sunRad = 1.5;
const double jupRad = 1.2;
const double satRad = 1.1;
const double uraRad = 0.9;
const double nepRad = 0.7;
const double earRad = 0.6;
const double venRad = 0.6;
const double marRad = 0.5;
const double merRad = 0.3;
const double pluRad = 0.3;
const double moonRad = 0.2;
const double ringRad = 0.3;
const double mercuryLocation = 2.5;
const double moonDist = 1.1;		// moon distance from earch
const double rotatePlanet = 0.5;	// roate speed planet

									// starting point for planets
static double mercuryRev = 0;
static double venusRev = 0;
static double earthRev = 0;
static double moonRev = 0;
static double marsRev = 0;
static double juputerRev = 0;
static double saturnRev = 0;
static double uranusRev = 0;
static double neptuneRev = 0;
static double plutoRev = 0;

static double rotation = 0;

/* This function sets up the properties for surface materials */
void properties()
{
	GLfloat mat_ambient[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat mat_diffuse[] = { 0.6f, 0.6f, 0.6f, 1.0f };
	GLfloat mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat mat_shininess[] = { 50.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glEnable(GL_COLOR_MATERIAL);
}

/* This function sets up the ligh source properties */
void setLightProperties()
{
	GLfloat lightIntensity[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat light_position[] = { 2.0f, 6.0f, 3.0f, 0.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightIntensity);
}


/* This function writes text on the screen */
void screenText(char *string)
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glDisable(GL_DEPTH_TEST); // Make text render on top.
	glDisable(GL_LIGHTING);
	glPushAttrib(GL_CURRENT_BIT);
	glColor3f(1, 1, 0);
	glRasterPos2f(-0.9, -0.9);
	char buf[100];
	sprintf_s(buf, string);
	const char * p = buf;
	void* font = GLUT_BITMAP_HELVETICA_18;
	for (char* c = string; *c != '\0'; ++c)
		glutBitmapCharacter(font, *c);
	glPopAttrib();
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

/* This function sets up the camera cam */
void setupCamera()
{
	eyeX = 0;
	eyeY = 0;
	eyeZ = 250;
	Point3 eye(eyeX, eyeY, eyeZ);
	cam.set(eye, look, up);

	double angle = 20;
	double aspectRatio = windowWidth / static_cast<double>(windowHeight);
	double nearD = 0.5;
	double farD = 500;
	cam.setShape(angle, aspectRatio, nearD, farD);
}

/* This function draws sphere */
void drawSphere(const int texture, const double radius, const double xTranslation = 0, const double zRevolutionRotation = 0, const double zRotationRotation = 0)
{
	glPushMatrix();
	glRotated(zRevolutionRotation, 0, 0, 1);
	glTranslated(xTranslation, 0, 0);
	glRotated(zRotationRotation, 0, 0, 1);
	GLUquadricObj *sphere = gluNewQuadric();
	gluQuadricTexture(sphere, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, texture);
	gluSphere(sphere, radius, 100, 100);			// number of slices 100
	glPopMatrix();
}

/* This funciton draws ring aroun Saturn */
void drawRings(const int texture, const double innerRadius, const double outerRadius, const double xTranslation = 0, const double zRevolutionRotation = 0, const double zRotationRotation = 0)
{
	glPushMatrix();

	glRotated(zRevolutionRotation, 0, 0, 1);
	glTranslated(xTranslation, 0, 0);
	glRotated(zRotationRotation, 0, 0, 1);

	GLUquadricObj *disk = gluNewQuadric();
	gluQuadricTexture(disk, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, texture);
	gluDisk(disk, innerRadius, outerRadius, 100, 100);		// 100 number of slices and loops

	glPopMatrix();
}

/* This function is for Moon around the Earth */
void drawMoon(const int texture, const double radius, const double xTranslation = 0, const double zRevolutionRotation = 0, const double zRotationRotation = 0, const int moonDistance = 0, const double moonRevolutionRotation = 0)
{
	glPushMatrix();
	glRotated(zRevolutionRotation, 0, 0, 1);
	glTranslated(xTranslation, 0, 0);
	glRotated(moonRevolutionRotation, 0, 0, 1);
	glTranslated(moonDistance, 0, 0);
	glRotated(zRotationRotation, 0, 0, 1);
	GLUquadricObj *sphere = gluNewQuadric();
	gluQuadricTexture(sphere, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, texture);
	gluSphere(sphere, radius, 100, 100);			// 100 number of slices and stacks
	glPopMatrix();
}

/* This function returns true if parameters matches current distance */
bool yesPlanet(const double currDistanceToSun, const double planetDistanceToSun, const double planetRadius)
{
	return (currDistanceToSun > planetDistanceToSun - planetRadius && currDistanceToSun < planetDistanceToSun + planetRadius);
}

/* This function returns planet or -1 if no planet is at specified location */
int locPlanet(const double posX, const double posY, const double posZ)
{
	double distanceToSun = sqrt(posX * posX + posY * posY);
	double farthestPossibleDistanceFromSun = mercuryLocation * 9 + pluRad;	// If we find any value outside of this, we are not clicking on a planet.

	if (posZ < 2 && posZ > 0.1 && distanceToSun <= farthestPossibleDistanceFromSun)	// Let's check which planet we clicked.
	{
		//cout << "posX: " << posX << " posY: " << posY << " posZ: " << posZ << endl;

		if (yesPlanet(distanceToSun, mercuryLocation, merRad))
			return 1;
		else if (yesPlanet(distanceToSun, mercuryLocation * 2, venRad))
			return 2;
		else if (yesPlanet(distanceToSun, mercuryLocation * 3, earRad))
			return 3;
		else if (yesPlanet(distanceToSun, mercuryLocation * 4, marRad))
			return 4;
		else if (yesPlanet(distanceToSun, mercuryLocation * 5, jupRad))
			return 5;
		else if (yesPlanet(distanceToSun, mercuryLocation * 6, satRad))
			return 6;
		else if (yesPlanet(distanceToSun, mercuryLocation * 7, uraRad))
			return 7;
		else if (yesPlanet(distanceToSun, mercuryLocation * 8, nepRad))
			return 8;
		else if (yesPlanet(distanceToSun, mercuryLocation * 9, pluRad))
			return 9;
		else
			return -1;
	}
	else
	{
		return -1;			// Didn't click on planet properly
	}
}

/* This function zoom in used in Key function */
void zoomIn()
{
	if (planetMode != 0)
		return;
	eyeZ -= zoom;
	Point3 eye(eyeX, eyeY, eyeZ);
	cam.set(eye, look, up);

	glRotated(rotX, 1, 0, 0);	// Rerotate the scene after the zoom has been applied.
	glRotated(rotY, 0, 1, 0);
	glRotated(rotZ, 0, 0, 1);
	glutPostRedisplay();
}

/* This function zoom out used in Key function */
void zoomOut()
{
	if (planetMode != 0)
		return;
	eyeZ += zoom;
	Point3 eye(eyeX, eyeY, eyeZ);
	cam.set(eye, look, up);

	glRotated(rotX, 1, 0, 0);
	glRotated(rotY, 0, 1, 0);
	glRotated(rotZ, 0, 0, 1);
}

/* This function is used when 'd' is pressed in Key function */
void reset()
{
	animation = false;
	properties();
	setupCamera();
	setLightProperties();
	rotX = 0, rotY = 0, rotZ = 0;
	mercuryRev = 0;
	venusRev = 0;
	earthRev = 0;
	moonRev = 0;
	marsRev = 0;
	juputerRev = 0;
	saturnRev = 0;
	uranusRev = 0;
	neptuneRev = 0;
	plutoRev = 0;
	rotation = 0;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_RGB);

	double orbit = 0.05;								// Width of orbits
	if (movement)
	{
		mercuryRev += 1.6;
		venusRev += 1.4;
		earthRev += 1.3;
		marsRev += 1.0;
		juputerRev += 0.9;
		saturnRev += 0.8;
		uranusRev += 0.7;
		neptuneRev += 0.4;
		plutoRev += 0.2;
		moonRev += 1.5;
	}

	if (mercuryRev >= 360)
		mercuryRev = 360 - mercuryRev;
	if (venusRev >= 360)
		venusRev = 360 - venusRev;
	if (earthRev >= 360)
		earthRev = 360 - earthRev;
	if (moonRev >= 360)
		moonRev = 360 - moonRev;
	if (marsRev >= 360)
		marsRev = 360 - marsRev;
	if (juputerRev >= 360)
		juputerRev = 360 - juputerRev;
	if (saturnRev >= 360)
		saturnRev = 360 - saturnRev;
	if (uranusRev >= 360)
		uranusRev = 360 - uranusRev;
	if (neptuneRev >= 360)
		neptuneRev = 360 - neptuneRev;
	if (plutoRev >= 360)
		plutoRev = 360 - plutoRev;

	if (!planetMode)							// This will stop orbit in singlePlantMode
	{
		drawSphere(2001, sunRad);			// Draw sun
											// Draw planets, moon, and rings.
		drawSphere(2002, merRad, mercuryLocation, mercuryRev, rotation);
		drawSphere(2003, venRad, mercuryLocation * 2, venusRev, rotation);
		drawSphere(2004, earRad, mercuryLocation * 3, earthRev, rotation);
		drawMoon(2012, moonRad, mercuryLocation * 3, earthRev, rotation, moonDist, moonRev);
		drawSphere(2005, marRad, mercuryLocation * 4, marsRev, rotation);
		drawSphere(2006, jupRad, mercuryLocation * 5, juputerRev, rotation);
		drawSphere(2007, satRad, mercuryLocation * 6, saturnRev, rotation);
		drawRings(2008, satRad + ringRad, satRad + 2 * ringRad, mercuryLocation * 6, saturnRev, rotation);
		drawSphere(2009, uraRad, mercuryLocation * 7, uranusRev, rotation);
		drawSphere(2010, nepRad, mercuryLocation * 8, neptuneRev, rotation);
		drawSphere(2011, pluRad, mercuryLocation * 9, plutoRev, rotation);
		// Draw orbits.
		glutSolidTorus(orbit, mercuryLocation, 100, 100);
		glutSolidTorus(orbit, mercuryLocation * 2, 100, 100);
		glutSolidTorus(orbit, mercuryLocation * 3, 100, 100);
		glutSolidTorus(orbit, mercuryLocation * 4, 100, 100);
		glutSolidTorus(orbit, mercuryLocation * 5, 100, 100);
		glutSolidTorus(orbit, mercuryLocation * 6, 100, 100);
		glutSolidTorus(orbit, mercuryLocation * 7, 100, 100);
		glutSolidTorus(orbit, mercuryLocation * 8, 100, 100);
		glutSolidTorus(orbit, mercuryLocation * 9, 100, 100);
	}
	else
	{
		rotation += rotatePlanet;
		moonRev += 1.5;
		if (moonRev >= 360)
			moonRev = 360 - moonRev;
		if (planetMode == 1)
		{
			drawSphere(2002, merRad, mercuryLocation, mercuryRev, rotation);
			screenText("You clicked Mercury!");
		}
		else if (planetMode == 2)
		{
			drawSphere(2003, venRad, mercuryLocation * 2, venusRev, rotation);
			screenText("You clicked Venus!");
		}
		else if (planetMode == 3)
		{
			drawMoon(2012, moonRad, mercuryLocation * 3, earthRev, rotation, moonDist, moonRev);
			drawSphere(2004, earRad, mercuryLocation * 3, earthRev, rotation);
			screenText("You clicked Earth!");
		}
		else if (planetMode == 4)
		{
			drawSphere(2005, marRad, mercuryLocation * 4, marsRev, rotation);
			screenText("You clicked Mars!");
		}
		else if (planetMode == 5)
		{
			drawSphere(2006, jupRad, mercuryLocation * 5, juputerRev, rotation);
			screenText("You clicked Jupiter!");
		}
		else if (planetMode == 6)
		{
			drawSphere(2007, satRad, mercuryLocation * 6, saturnRev, rotation);
			drawRings(2008, satRad + ringRad, satRad + 2 * ringRad, mercuryLocation * 6, saturnRev, rotation);
			screenText("You clicked Saturn!");
		}
		else if (planetMode == 7)
		{
			drawSphere(2009, uraRad, mercuryLocation * 7, uranusRev, rotation);
			screenText("You clicked Uranus!");
		}
		else if (planetMode == 8)
		{
			drawSphere(2010, nepRad, mercuryLocation * 8, neptuneRev, rotation);
			screenText("You clicked Neptune!");
		}
		else if (planetMode == 9)
		{
			drawSphere(2011, pluRad, mercuryLocation * 9, plutoRev, rotation);
			screenText("You clicked Pluto!");
		}
	}
	glFlush();
	Sleep(15);
	glutPostRedisplay();
}

void SpecialKey(int key, int x, int y)
{
	if (!animation) {
		cam.slide(0, 0, 0);			// slide camera
		animation = true;
	}
	if (planetMode != 0)
		return;

	switch (key)
	{
	case GLUT_KEY_UP:
		rotX += 10;
		glRotated(10, 1, 0, 0);
		glutPostRedisplay();
		break;
	case GLUT_KEY_DOWN:
		rotX -= 10;
		glRotated(-10, 1, 0, 0);
		glutPostRedisplay();
		break;
	case GLUT_KEY_LEFT:
		rotY -= 10;
		glRotated(-10, 0, 1, 0);
		glutPostRedisplay();
		break;
	case GLUT_KEY_RIGHT:
		rotY += 10;
		glRotated(10, 0, 1, 0);
		glutPostRedisplay();
		break;
	case GLUT_KEY_PAGE_UP:
		rotZ += 10;
		glRotated(10, 0, 0, 1);
		break;
	case GLUT_KEY_PAGE_DOWN:
		rotZ -= 10;
		glRotated(-10, 0, 0, 1);
		break;
	}
}

/* This function controls the actions of our solar system */
void Key(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 's':
		if (planetMode != 0)
			return;
		if (!movement)
			movement = true;
		else
			movement = false;
		break;
	case 'd':
		glRotated(-rotX, 1, 0, 0);
		glRotated(-rotY, 0, 1, 0);
		glRotated(-rotZ, 0, 0, 1);
		movement = true;
		planetMode = 0;
		reset();
		break;
	case 'z':
		zoomIn();
		break;
	case 'Z':
		zoomOut();
		break;
	case 'e':
		exit(0);
		break;
	}
	glutPostRedisplay();					// redisplay solar system as of options
}

void processMouse(int button, int state, int mouseX, int mouseY)
{
	int clickedPlanet;
	double x, y, z;	// Unprojected coordinates.
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && movement == false)
	{
		if (planetMode != 0)
			return;

		double modelview[16];
		double projection[16];
		int viewport[4];

		glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
		glGetDoublev(GL_PROJECTION_MATRIX, projection);
		glGetIntegerv(GL_VIEWPORT, viewport);

		float winX = static_cast<float>(mouseX);
		float winY = static_cast<float>(viewport[3]) - static_cast<float>(mouseY);
		float winZ;
		glReadPixels(mouseX, static_cast<int>(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
		gluUnProject(winX, winY, winZ, modelview, projection, viewport, &x, &y, &z);

		clickedPlanet = locPlanet(x, y, z);

		if (clickedPlanet)
		{
			if (clickedPlanet != 1 && clickedPlanet != 2 && clickedPlanet != 3 && clickedPlanet != 4 &&
				clickedPlanet != 5 && clickedPlanet != 6 && clickedPlanet != 7 && clickedPlanet != 8 && clickedPlanet != 9)
			{
				glutIdleFunc(zoomIn);
				return;
			}

			eyeX = x / 2.0;
			eyeY = y / 2.0;
			eyeZ = 0;

			if (clickedPlanet == 6)
				eyeZ = 0.2;												// Make a little bit of tilt to see the rings.
			if (clickedPlanet == 9 || clickedPlanet == 8)
				eyeX = x / 1.1;
			Point3 eye(eyeX, eyeY, eyeZ);
			Point3 lookPlanet(x, y, 0);

			cam.set(eye, lookPlanet, up);
			cam.roll(90);
			if (x < 0)
				cam.roll(180);

			planetMode = clickedPlanet;
			movement = false;
		}
		else
		{
			glutIdleFunc(zoomIn);
		}
	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		glutIdleFunc(zoomOut);
	}
	else
	{
		glutIdleFunc(NULL);
	}
}

/* This is my Initialization */
void myInit(void)
{
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

	/* Load images and texture of appropriate object */
	sun.readBMPFile("sun.bmp");
	sun.setTexture(2001);
	pix[0].readBMPFile("mercury.bmp");
	pix[0].setTexture(2002);
	pix[1].readBMPFile("venus.bmp");
	pix[1].setTexture(2003);
	pix[2].readBMPFile("earth.bmp");
	pix[2].setTexture(2004);
	pix[3].readBMPFile("mars.bmp");
	pix[3].setTexture(2005);
	pix[4].readBMPFile("jupiter.bmp");
	pix[4].setTexture(2006);
	pix[5].readBMPFile("saturn.bmp");
	pix[5].setTexture(2007);
	saturnRing.readBMPFile("saturnrings.bmp");
	saturnRing.setTexture(2008);
	pix[6].readBMPFile("uranus.bmp");
	pix[6].setTexture(2009);
	pix[7].readBMPFile("neptune.bmp");
	pix[7].setTexture(2010);
	pix[8].readBMPFile("pluto.bmp");
	pix[8].setTexture(2011);
	moon.readBMPFile("moon.bmp");
	moon.setTexture(2012);
	pix[9].readBMPFile("stars.bmp");
	pix[9].setTexture(2013);

	glEnable(GL_NORMALIZE);
	glClearColor(0.1f, 0.1f, 0.1f, 0.0f);
	glViewport(0, 0, windowWidth, windowHeight);
	reset();												// call reset function for original position
}

/* My Main Function */
void main(int argc, char** argv)
{
	glutInit(&argc, argv);										// glut toolkit	
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);	// type of buffer to use
	glutInitWindowSize(windowWidth, windowHeight);		// set up window
	glutInitWindowPosition(10, 50);		// position windows
	glutCreateWindow("My Solar System");
	glutDisplayFunc(display);			// Set display mode
	glutKeyboardFunc(Key);				// control actions
	glutSpecialFunc(SpecialKey);		// Handles arrow keys
	glutMouseFunc(processMouse);
	myInit();							// Call my initialization
	glutMainLoop();						// Go in infinitely event-handling loop
}
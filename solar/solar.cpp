﻿/* Muris Fazlic */
/* cs4410 HW 4  */
/* 11/12/2014   */

#include <Windows.h>
#include <gl/Gl.h>
#include <gl/Glu.h>
#include <GL/glut.h>
#include <math.h>
#include "RGBpixmap.h"

#define M_PI 3.1415926535897932384626433832795
#define AU 100

bool stopmode = false, left_down = false, right_down = false;
int currentDay = 0, w = 1024, h = 720;
float currentHour = 0.0, tInc = 0.2, zoom = 1.6, y = 300.0;
GLfloat angle = 0.0, x = 0.0, z = 5.0, lx = 0.0, ly = 0.0, lz = -1.0, swivel = 0, fraction;
GLUquadricObj* quadro;
RGBpixmap pix[12];

//Function prototypes
void initSystem();
void loadTexture();
void setCamera();
void reset();
void stop();
void animate();
void beginText();
void endtext();
void drawString(char *);
void display();
void drawOrbit(float);
void drawRings();
void Key(unsigned char, int, int);
void SpecialKey(int, int, int);
void myMouse(int, int, int, int);
void mouseDown(int, int);


//Function definitions
void initSystem()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glColor3f(1.0, 1.0, 1.0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void loadTexture()
{
	pix[0].readBMPFile("sunmap.bmp");
	pix[0].setTexture(0);
	pix[1].readBMPFile("mercurymap.bmp");
	pix[1].setTexture(1);
	pix[2].readBMPFile("venusmap.bmp");
	pix[2].setTexture(2);
	pix[3].readBMPFile("earthmap.bmp");
	pix[3].setTexture(3);
	pix[4].readBMPFile("moonMap.bmp");
	pix[4].setTexture(4);
	pix[5].readBMPFile("marsmap.bmp");
	pix[5].setTexture(5);
	pix[6].readBMPFile("jupitermap.bmp");
	pix[6].setTexture(6);
	pix[7].readBMPFile("saturnmap.bmp");
	pix[7].setTexture(7);
	pix[8].readBMPFile("saturnringpattern.bmp");
	pix[8].setTexture(8);
	pix[9].readBMPFile("uranusmap.bmp");
	pix[9].setTexture(9);
	pix[10].readBMPFile("neptunemap.bmp");
	pix[10].setTexture(10);
	pix[11].readBMPFile("plutomap.bmp");
	pix[11].setTexture(11);
}

void setCamera()
{
	glMatrixMode(GL_PROJECTION);
	gluPerspective(60.0 * zoom, w/h, 1.0, 500.0);

	glMatrixMode(GL_MODELVIEW);
	//gluLookAt(0.0, 300.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
	//gluLookAt(x, 300.0, z, x+lx, 0.0, z+lz, 0.0, 0.0, 1.0);
	gluLookAt(x, y, z, x + lx, ly, z + lz, 0.0, 0.0, 1.0);
}

//Reset after pressing 'D'
void reset()
{
	zoom = 1.6;
	y = 300;
	angle = 0.0;
	x = 0.0;
	z = 5.0;
	lx = 0.0;
	ly = 0.0;
	lz = -1.0;
	swivel = 0;
	stopmode = false;
	initSystem();
	setCamera();
}

//Toggles the animation
void stop()
{
	if (stopmode)
		stopmode = false;
	else stopmode = true;
}

//Revolution around the sun
void animate()
{

	if (stopmode)
		return;

	currentHour += tInc;

	if (currentHour >= 24.0)
	{
		currentHour = 0.0;
		currentDay += 1;
		currentDay %= 365;
	}

	glutPostRedisplay();
}

void beginText()
{
	glPushMatrix();
}

void endText()
{
	glPopMatrix();
}

void drawString(char *string)
{
	glRasterPos2i(10, 10);
	void *font = GLUT_BITMAP_HELVETICA_18;
	for (char* c = string; *c != '\0'; c++)
	{
		glutBitmapCharacter(font, *c);
	}
}

//The meat of the program, draw all the planets and sun
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	beginText();
	glColor3f(1.0, 0.0, 0.0);
	glTranslated(1024.0, 720, 0);
	drawString("Solar System");
	glColor3f(1.0, 1.0, 1.0);
	endText();

	GLUquadricObj* quadro = gluNewQuadric();
	gluQuadricNormals(quadro, GLU_SMOOTH);
	gluQuadricTexture(quadro, GL_TRUE); 
	glEnable(GL_TEXTURE_2D);

	//Draw sun
	glPushMatrix();
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glPushMatrix();
			glRotated(90.0, 1.0, 0.0, 0.0);
			glBindTexture(GL_TEXTURE_2D, 0);
			gluSphere(quadro, 42, 40, 40);
		glPopMatrix();

		//Draw Mercury
		glPushMatrix();
			drawOrbit(50);
			glRotated(((double)currentDay + currentHour/24.0)/2.0 * 360.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, 50);
			glPushMatrix();
				glRotated(currentHour / 12.0 * 360.0, 0.0, 1.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 1);
				gluSphere(quadro, 3.8, 40, 40);
			glPopMatrix();
		glPopMatrix();

		//Draw Venus
		glPushMatrix();
			drawOrbit(72);
			glRotated(((double)currentDay + currentHour/24.0)/4.7 * 360.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, 72);
			glPushMatrix();
				glRotated(currentHour / 24.0 * 360.0, 0.0, 1.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 2);
				gluSphere(quadro, 8.5, 40, 40);
			glPopMatrix();
		glPopMatrix();

		//Draw Earth
		glPushMatrix();
			drawOrbit(AU);
			glRotated(((double)currentDay + currentHour/24.0)/15.0 * 360.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, AU);
			glPushMatrix();
				glRotated(6.0, 0.0, 0.0, 1.0);
				drawOrbit(20);
				glRotated(currentHour / 24.0 * 360.0, 0.0, 1.0, 0.0);
				glRotated(90.0, 1.0, 0.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 3);
				gluSphere(quadro, 9, 40, 40);
			glPopMatrix();

			//Draw moon
			glPushMatrix();
				glRotated(6.0, 0.0, 0.0, 1.0);
				glRotated(((double)(currentDay % 28) + currentHour/24.0)/28.0 * 360.0, 0.0, 1.0, 0.0);
				glTranslated(0.0, 0.0, 20.0);
				glRotated(90.0, 1.0, 0.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 4);
				gluSphere(quadro, 2.0, 40, 40);
			glPopMatrix();
		glPopMatrix();

		//Draw Mars
		glPushMatrix();
			drawOrbit(172);
			glRotated(((double)currentDay + currentHour/24.0)/25.93 * 360.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, 172);
			glPushMatrix();
				glRotated(currentHour / 24.0 * 360.0, 0.0, 1.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 5);
				gluSphere(quadro, 5.3, 40, 40);
			glPopMatrix();
		glPopMatrix();

		//Draw Jupiter
		glPushMatrix();
			drawOrbit(200);
			glRotated(((double)currentDay + currentHour/24.0)/30.6 * 360.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, 200);
			glPushMatrix();
				glRotated(currentHour / 24.0 * 360.0, 0.0, 1.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 6);
				gluSphere(quadro, 15, 40, 40);
			glPopMatrix();
		glPopMatrix();

		//Draw Saturn
		glPushMatrix();
			drawOrbit(220);
			glRotated(((double)currentDay + currentHour/24.0)/55.7 * 360.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, 220);
			glPushMatrix();
				glRotated(currentHour / 24.0 * 360.0, 0.0, 1.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 7);
				gluSphere(quadro, 15, 40, 40);
			glPopMatrix();
		glPopMatrix();
		//drawRings();

		//Draw Uranus
		glPushMatrix();
			drawOrbit(240);
			glRotated(((double)currentDay + currentHour/24.0)/87 * 360.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, 240);
			glPushMatrix();
				glRotated(currentHour / 24.0 * 360.0, 0.0, 1.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 9);
				gluSphere(quadro, 12, 40, 40);
			glPopMatrix();
		glPopMatrix();

		//Draw Neptune
		glPushMatrix();
			drawOrbit(280);
			glRotated(((double)currentDay + currentHour/24.0)/(165) * 360.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, 280);
			glPushMatrix();
				glRotated(currentHour / 24.0 * 360.0, 0.0, 1.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 10);
				gluSphere(quadro, 11, 40, 40);
			glPopMatrix();
		glPopMatrix();

		//Draw Pluto
		glPushMatrix();
			drawOrbit(330);
			glRotated(((double)currentDay + currentHour/24.0)/(248) * 360.0, 0.0, 1.0, 0.0);
			glTranslated(0.0, 0.0, 330);
			glPushMatrix();
				glRotated(currentHour / 24.0 * 360.0, 0.0, 1.0, 0.0);
				glBindTexture(GL_TEXTURE_2D, 11);
				gluSphere(quadro, 2.0, 40, 40);
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();
	
	glutSwapBuffers();
}

//Draw the elliptical orbit of planets
void drawOrbit(float radius)
{
	int i;
	glBegin(GL_LINE_LOOP);
	for (i = 0; i < 40; i++)
	{
		glVertex3d(radius * sin(2 * M_PI/40.0 * (double)i), 0.0, radius * cos(2 * M_PI/40.0 * (double)i));
	}
	glEnd();
}

//Draw the rings of Saturn
void drawRings()
{
	GLUquadricObj* quadric = gluNewQuadric();							
	gluQuadricNormals(quadric, GLU_SMOOTH);		
	gluQuadricTexture(quadric, GL_TRUE);			
	glEnable(GL_TEXTURE_2D);
		glPushMatrix();
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
			glPushMatrix();
				glRotatef(2, 0.0, 0.0, 1.0);
				glRotatef( 360.0 * (currentDay/0.4), 0.0, 1.0, 0.0);
				glTranslatef(180, 0.0, 0.0 );
				glRotatef( -90.0, 1.0, 0.0, 0.0 );
				glScalef(1,1,.02);
				glBindTexture(GL_TEXTURE_2D, 8);
				gluSphere(quadric, 0.5*2, 48, 48);
			glPopMatrix();
		glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	gluDeleteQuadric(quadric);
}

//Keyboard handlers
void Key(unsigned char key, int x, int y)
{
	switch(key)
	{
		//Reset, 'D'
		case 100:
			reset();
			break;
		//Toggle animation, 'S'
		case 115:
			stop();
			break;
		//End the program, 'E'
		case 101:
			exit (0);
	}
}

void SpecialKey(int Key, int mouseXPosition, int mouseYPosition)
{
	fraction = 0.1f;
	switch(Key)
	{
		case GLUT_KEY_RIGHT: {
			swivel -= 0.5f;
			lx = 150*sin(swivel);
			initSystem();
			setCamera();
			break;
		}
		case GLUT_KEY_LEFT: {
			swivel += 0.5f;
			lx = 150*sin(swivel);
			initSystem();
			setCamera();
			break;
		}
		case GLUT_KEY_UP:
		{
			angle -= 0.5f;
			lx = 150*sin(angle);
			y = 150*sin(angle);
			lz = -150*cos(angle);
			initSystem();
			setCamera();
			break;
		}
		case GLUT_KEY_DOWN:
		{
			angle += 0.5f;
			lx = 150*sin(angle);
			y = 150*sin(angle);
			lz = -150*cos(angle);
			initSystem();
			setCamera();
			break;
		}
	}
}

//Zoom in with left mouse button, zoom out with right mouse button
void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		left_down = true;
		right_down = false;
		if (zoom > 0)
			zoom -= 0.2;
	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		left_down = false;
		right_down = true;
		if (zoom < 2.0)
			zoom += 0.2;
	}

	initSystem();
	setCamera();
}

void mouseDown(int x, int y)
{
	if (left_down)
	{
		if (zoom > 0)
			zoom -= 0.2;
	}
	if (right_down)
	{
		if (zoom < 2.0)
			zoom += 0.2;
	}

	initSystem();
	setCamera();
}

void main(int argc, char *argv[])
{
	//Standard usual initialization and set-up
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(w, h);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Solar System");

	quadro = gluNewQuadric();
	gluQuadricNormals(quadro, GLU_SMOOTH);		
	gluQuadricTexture(quadro, GL_TRUE);	

	//Handlers
	glutKeyboardFunc(Key);
	glutMouseFunc(myMouse);
	glutMotionFunc(mouseDown);
	glutSpecialFunc(SpecialKey);
	glutDisplayFunc(display);
	glutIdleFunc(animate);

	initSystem();
	setCamera();
	loadTexture();
	glutMainLoop();
}
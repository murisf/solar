/* Muris Fazlic */
/* cs4410 HW 4  */
/* 11/12/2014   */

#include <Windows.h>
#include <gl/Gl.h>
#include <gl/Glu.h>
#include <gl/glut.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class RGB
{
	public: unsigned char r, g, b;
};

class RGBpixmap
{
	public:
		int nRows, nCols;
		RGB* pixel;
		int readBMPFile(string fname);
		void setTexture(GLuint textureName);
};